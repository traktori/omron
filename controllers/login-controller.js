'use strict';

var jwt = require('jsonwebtoken');
var User = require('../models/user-model.js');
const ERROR = require('../config/errors.js');

var secret;
module.exports = (app) => {
	secret = app.get('secret');
	app.post('/login', login);
}

function login(req, res, next) {

	if (!req.body.email) return next({status: 400, code: ERROR.EMAIL_MISSING});
	if (!req.body.password) return next({status: 400, code: ERROR.PASSWORD_MISSING});

	User.findOne({email: req.body.email}, '+password').exec()
		.then(user => {

			if (!user) return next({status: 401, code: ERROR.USER_NOT_FOUND});
			
			user.comparePassword(req.body.password, function(err, match) {
	
				if (!match) return next({status: 401, code: ERROR.WRONG_PASSWORD});
		
		        var token = jwt.sign({id: user._id}, secret);
				res.json({token: token});
			});
		})
		.catch(err => {
			next({code: ERROR.LOGIN_FAILED});
		})

}