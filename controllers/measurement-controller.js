'use strict';

var Promise = require('bluebird');
var Measurement = require('../models/measurement-model.js');
const CONST = require('../config/constants.js');
const ERROR = require('../config/errors.js');

module.exports = (app) => {
	var router = require('express').Router();
	app.use(CONST.MEASUREMENT_ROUTE, router);
	router.get('/', getNumberOfMeasurements);
    router.get('/:limit', getLatestMeasurements);
    router.get('/all', getAllMeasurements);
	router.post('/', saveMeasurement);
	router.put('/:id', modifyMeasurement);
	router.delete('/:id', deleteMeasurement)
}

function getNumberOfMeasurements(req, res) {
	Measurement.count({'user_id': req.user.id})
		.then(count => { res.json({'count': count}); })
		.catch(err => { res.sendStatus(500)});
}

function getAllMeasurements(req, res) {
    Measurement.find({'user_id': req.user.id})
        .sort({date: -1})
        .exec()
        .then(measurement => { res.json(measurement); })
        .catch(err => { res.sendStatus(500) });
}

function getLatestMeasurements(req, res) {
	Measurement.find({'user_id': req.user.id})
		.sort({date: -1})
		.limit(parseInt(req.params.limit))
		.exec()
		.then(measurement => { res.json(measurement); })
		.catch(err => { res.sendStatus(500) });
}

function saveMeasurement(req, res, next) {
	
	if (!req.body.date) return next({status: 400, code: ERROR.DATE_MISSING});
	if (!req.body.weight) return next({status: 400, code: ERROR.WEIGHT_MISSING});

	var measurement = Measurement({
	  	user_id: req.user.id,
  		date: req.body.date,
		weight: req.body.weight,
		bmi: req.body.bmi,
		fat_percentage: req.body.fat_percentage,
		muscle_percentage: req.body.muscle_percentage,
		resting_metabolism: req.body.resting_metabolism,
		visceral_fat: req.body.visceral_fat
	});

	measurement.save()
		.then(created => {
			res.json(created);
		})
		.catch(err => {
			next(err);
		});
}

function modifyMeasurement(req, res) {
	var measurement = Measurement({
		_id: req.params.id,
  		date: req.body.date,
		weight: req.body.weight,
		bmi: req.body.bmi,
		fat_percentage: req.body.fat_percentage,
		muscle_percentage: req.body.muscle_percentage,
		resting_metabolism: req.body.resting_metabolism,
		visceral_fat: req.body.visceral_fat
	});
	
	Measurement.findByIdAndUpdate(measurement._id, measurement, {new: true}).exec()
		.then(mea => { res.json(mea) })
		.catch(err => { res.sendStatus(500) });			
}

function deleteMeasurement(req, res) {
	Measurement.findByIdAndRemove(req.params.id).exec()
		.then(measurement => { res.json(measurement) })
		.catch(err => { res.sendStatus(500) });
}