'use strict';

var Promise = require('bluebird');
var User = require('../models/user-model.js');
const CONST = require('../config/constants.js');
const ERROR = require('../config/errors.js');

module.exports = (app) => {
	var router = require('express').Router();
	app.use(CONST.USER_ROUTE, router);

	// Signup
	app.post('/user', createUser);

	// Protected
	router.get('/', getUsers);
	router.get('/me', getUserById);
	router.delete('/', deleteUser);
	router.put('/', modifyUser);
}

function createUser(req, res, next) {

	if (!req.body.email) return next({status: 400, code: ERROR.EMAIL_MISSING});
	if (!req.body.password) return next({status: 400, code: ERROR.PASSWORD_MISSING});

	var user = User({
	  	email: req.body.email,
  		password: req.body.password
	});

	user.save()
		.then(created => {
			res.json({_id: created._id});
		})
		.catch(err => {
			next({status: 500, code: err.code});
		});
}

function getUsers(req, res, next) {
	User.find().lean().exec()
		.then(users => { 
			res.json(users);
		})
		.catch(err => { next(err) });
}

function getUserById(req, res, next) {
	User.findById(req.user.id).lean().exec()
		.then(user => { 
			res.json(user);
		})
		.catch(err => { next(err) });
}

function deleteUser(req, res, next) {
	User.findByIdAndRemove(req.user.id).lean().exec()
		.then(user => { 
			res.json({_id: user._id});
		})
		.catch(err => { next(err) });
}

function modifyUser(req, res, next) {

	if (!req.body.gender) return next({status: 400, code: ERROR.GENDER_MISSING});
	if (!req.body.birthdate) return next({status: 400, code: ERROR.BIRTHDATE_MISSING});
	if (!req.body.height) return next({status: 400, code: ERROR.HEIGHT_MISSING});

	var user = User({
		_id: req.user.id,
		gender: parseInt(req.body.gender),
		birthdate: new Date(req.body.birthdate),
		height: parseInt(req.body.height)
	});

	User.findByIdAndUpdate(req.user.id, user, {new: true}).lean().exec()
		.then(user => {
			if (!user) return next({success: false, code: ERROR.USER_NOT_FOUND});
		 	res.json(user);
		})
		.catch(err => { 
			next({status: 500});
		});		
}