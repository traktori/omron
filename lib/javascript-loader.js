'use strict';

const Promise = require('bluebird');
const fs = require('fs-extra');
const path = require('path');

module.exports = (dir, app) => {
	return Promise.map(fs.readdirSync(dir), (file) =>  {
		if (path.extname(file) == '.js') {
			require(dir + file)(app);
		}
	});
}