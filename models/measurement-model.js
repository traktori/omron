'use strict';

var mongoose = require('../lib/database.js');
var Schema = mongoose.Schema;

var measurementSchema = new Schema({
	user_id: {type: Schema.Types.ObjectId, required: true},
	date: {type: Date, required: true},
	weight: {type: Number, required: true},
	bmi: Number,
	fat_percentage: Number,
	muscle_percentage: Number,
	visceral_fat: Number,
	resting_metabolism: Number,
	created_at: Date,
	updated_at: Date
});

module.exports = mongoose.model('Measurement', measurementSchema);