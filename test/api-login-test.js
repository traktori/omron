'use strict';

var request = require('supertest-as-promised');
var expect = require('chai').expect;

var app = require('../app.js');
const CONST = require('../config/constants.js');
const ERROR = require('../config/errors.js');
var User = require('../models/user-model.js');

var TEST_USER = {
	email: 'test_username',
	password: '12345678'
};

/*********************************************************************************************/
/* LOGIN API - implemented in login-controller.js
/*********************************************************************************************/
describe('Login API', () => {

	before(() => {
		var user = User({
			email: TEST_USER.email,
			password: TEST_USER.password
		});
		return user.save();
	});

	after(() => {
		return User.findOneAndRemove({ email: TEST_USER.email }).exec();
	})

	it('should return correct error when email is missing', () => {
		return request(app)
			.post(CONST.LOGIN_ROUTE)
			.expect(400, {
				success: false,
				code: ERROR.EMAIL_MISSING
			});
	});

	it('should return correct error when password is missing', () => {
		return request(app)
			.post(CONST.LOGIN_ROUTE)
			.send({email: TEST_USER.email})
			.expect(400, {
				success: false,
				code: ERROR.PASSWORD_MISSING
			});
	});

	it('should return correct response when user is not found', () => {
		return request(app)
			.post('/login')
			.send({email:'somedummyuser@user.fi', password: '321321321'})
			.expect(401, {
				success: false,
				code: ERROR.USER_NOT_FOUND
			});
	});

	it('should return correct response when password is wrong', () => {
		return request(app)
			.post('/login')
			.send({email: TEST_USER.email, password: TEST_USER.password + 'something'})
			.expect(401, {
				success: false,
				code: ERROR.WRONG_PASSWORD
			});				
	});

	it('should return correct response when the login is successful', () => {
		return request(app)
			.post('/login')
			.send({email: TEST_USER.email, password: TEST_USER.password})
			.expect(200)
			.then(res => {
				expect(res.body.token).not.to.be.empty;
			});
	});
});